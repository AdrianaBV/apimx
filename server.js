var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var session = require('express-session')

var path = require('path');

var requestjson = require('request-json');

var bcrypt = require('bcryptjs');

var urlMLabRaiz =  "https://api.mlab.com/api/1/databases/abasurto/collections"
var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLabRaiz;

var urlClientes = "https://api.mlab.com/api/1/databases/abasurto/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"
var clienteMLab = requestjson.createClient(urlClientes)


var bodyparser = require('body-parser');   //Realizar modificaciones
app.use(bodyparser.json());
app.use(function(req, res, next){
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Headers", "Origin, X-Requeste-With, Content-Type, Accept")
  next()
});
app.use(session({
  secret:'secret',
  resave: true,
  saveUninitialized: true
}))

var movimientosJSON = require('./movimientosv2.json');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send("Hola mundo desde NodeJS");
  res.sendFile(path.join(__dirname,'index.html'));
})

app.post('/', function(req, res) {
  res.send("Hemos recibido su petición cambiada");
})

app.put('/', function(req, res) {
  res.send("Hemos recibido su petición put");
})

app.delete('/', function(req, res) {
  res.send("Hemos recibido su petición delete");
})

app.get('/Clientes/:idcliente', function(req, res) {
  res.send("Aqui tiene al cliente numero " + req.params.idcliente);
})

app.get('/v1/Movimientos', function(req, res) {
  res.sendfile(path.join('movimientosv1.json'));
})

app.get('/v2/Movimientos', function(req, res) {
  res.json(movimientosJSON);
})

app.get('/v2/Movimientos/:id', function(req, res) {
  console.log(req.params.id);
  res.send(movimientosJSON[req.params.id-1]);
})

app.get('/v2/MovimientosQuery', function(req, res) {
  console.log(req.query);
  res.send("Se recibio el query");
})

app.post('/v2/Movimientos', function(req, res) {
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length + 1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})

app.post('/Clientes',function(req, res){
  clienteMLab.post('', req.body, function(err, resM, body){
    res.send(body)
  })
})

app.post('/Login',function(req, res){
  //res.set("Access-Control-Allow-Headers", "Content-Type")
  var email = req.body.email
  var password = req.body.password

  var query = 'q={"email":"'+email+'"}'
  console.log(query)
  var urlMLab= urlMLabRaiz+'/Usuarios?'+query+"&"+apiKey
  console.log(urlMLab)
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('', function(err, resM, body){
    if(!err){
      if(body.length){
        var temp=bcrypt.compareSync(password,body[0].password)
        if(temp){//Loging OK, se encontro 1 documento
          req.session.loggedin = true
          req.session.user = email
          if(email=="admin"){req.session.admin=true}
          res.status(200).send(temp)
        } else{
          res.status(404).send(err)
        }
      }else{
        res.status(404).send("Usuario no encontrado")
      }
    }
  })
})

app.post('/Signup',function(req, res){
  //if(req.session.loggedin && req.session.username=="admin"){
  var nuevo = req.body
  var email = nuevo.email
  var urlMLab= urlMLabRaiz+'/Usuarios?'+apiKey
    clienteMLabRaiz = requestjson.createClient(urlMLab)
    clienteMLabRaiz.get('',function(err, resM, body){
      if(!err){
        if(body.length){
          nuevo.idcliente=body.length+10000000
        }else{nuevo.idcliente=10000000}
        var query = 'q={"email":"'+email+'"}'
        urlMLab= urlMLabRaiz+'/Usuarios?'+query+"&"+apiKey
        clienteMLabRaiz = requestjson.createClient(urlMLab)
        clienteMLabRaiz.get('',function(err2, resM2, body2){
        if(!err2){
          if(body2.length){
            res.status(404).send('Usuario ya Registrado')
          }else {
            nuevo.password=bcrypt.hashSync(nuevo.password,bcrypt.genSaltSync(10))
            urlMLab= urlMLabRaiz+'/Usuarios?'+apiKey
            clienteMLabRaiz = requestjson.createClient(urlMLab)
            clienteMLabRaiz.post('',nuevo,function(err3, resM3, body3){
              if(!err3){
                res.status(200).send(body3)
              }else{
                res.status(404).send('Error al Registrar Usuario')
              }
            })
          }
        }
      })
    }
  })
})
app.post('/ContratarCuenta',function(req, res){
  //if(req.session.loggedin && req.session.username=="admin"){
  var nuevo = req.body
  nuevo.listado=[]
  var cuenta = parseInt(nuevo.idcuenta)
  var query = 'q={"idcuenta":'+cuenta+'}'
  var urlMLab= urlMLabRaiz+'/Clientes?'+query+"&"+apiKey
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('',function(err, resM, body){
    if(!err){
      if(body.length){
        res.status(404).send('Cuenta ya Registrada')
      }else {
        urlMLab= urlMLabRaiz+'/Clientes?'+apiKey
        clienteMLabRaiz = requestjson.createClient(urlMLab)
        clienteMLabRaiz.post('',nuevo,function(err3, resM3, body3){
          if(!err3){
            res.status(200).send(body3)
          }else{
            res.status(404).send('Error al contratar cuenta')
          }
        })
      }
    }
  })

})


app.post('/AltaMovimientos',function(req,res){
  //if(req.session.loggedin && req.session.username==req.body.email){
    var query = 'q={"idcuenta":'+req.body.idcuenta+'}'
    var listado = req.body.listado
    //var mov = '{"fecha":"'+listado.fecha+'"","importe":'+listado.importe+',"categoria":"'+listado.categoria+'"}'
    var urlMLab =urlMLabRaiz+'/Clientes?'+query+"&"+apiKey
    clienteMLabRaiz = requestjson.createClient(urlMLab)
    clienteMLabRaiz.get('',function(err,resM,body){
      if(!err){
        if(body.length){
          listado.id=body[0].listado.length+1
          listado.importe=listado.importe
          var saldo=req.body.saldo
          var insertar = {"$push":{"listado":{"$each":[listado],"$position":0}},"$set":{"saldo":saldo}}
          clienteMLabRaiz.put('',insertar,function(err2,resM2,body2){
            if(!err2){
              res.status(200).send(body)
            }else{
              res.status(404).send
            }
          })
        }else{
          res.status(404).send("No se encontro la cuenta")
        }
      }
    })
})

app.post('/ConsultarMovimientos',function(req,res){
  var nuevo = req.body
  var inicio =nuevo.inicio
  var fin =nuevo.fin
  var categoria = nuevo.categoria
  var idcuenta = nuevo.idcuenta
  var query = 'q={"listado.categoria":"'+categoria+'","idcuenta":'+idcuenta+'}&f={"listado.$":1}'
  if(!categoria){
    query = 'q={"idcuenta":'+idcuenta+'}&f={"listado":1}'
  }

  var urlMLab =urlMLabRaiz+'/Clientes?'+query+'&'+apiKey
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
      if(body.length){
        res.status(200).send(body)
      }else{
        res.status(404).send(query)
      }
    }
  })
})

app.get('/ConsultarSaldo/:idcuenta',function(req,res){
  var query = 'q={"idcuenta":'+req.params.idcuenta+'}&f={"saldo":1}'
  var urlMLab =urlMLabRaiz+'/Clientes?'+query+"&"+apiKey
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
      if(!body.length){
        res.status(404).send("Error de Cuenta")
      }
      res.status(200).send(body[0])
    }
  })
})
app.get('/ObtenerCliente/:usuario',function(req,res){
  var query = 'q={"email":"'+req.params.usuario+'"}&f={"nombre":1,"idcliente":1}'
  var urlMLab =urlMLabRaiz+'/Usuarios?'+query+"&"+apiKey
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
      if(body.length){
        res.status(200).send(body)
      }
      else{
        res.status(404).send("No se encuentra al usuario")
      }

    }
  })
})
app.get('/ObtenerCuenta/:cliente',function(req,res){
  var query = 'q={"cliente":'+req.params.cliente+'}&f={"idcuenta":1}'
  var urlMLab =urlMLabRaiz+'/Clientes?'+query+"&"+apiKey
  clienteMLabRaiz = requestjson.createClient(urlMLab)
  clienteMLabRaiz.get('',function(err,resM,body){
    if(!err){
      if(body.length){
        res.status(200).send(body)
      }
      else{
        res.status(404).send("No se encuentra al usuario")
      }

    }
  })

})
app.get('/logout',function(req,res){
  req.session.destroy()
  res.send("logout success")
})
